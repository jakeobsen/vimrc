" Vundle
" source ~/.vim/vundlerc

" Vundle config
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-scripts/vim-auto-save'
call vundle#end()
filetype plugin indent on

" Vundle required params
set nocompatible
filetype off

" Set line numbers
set number
set relativenumber

" Disable mouse
set mouse=""

" Enable syntax highlighting
syntax on

" Keep backup and undo files central
set backupdir=~/.vim/backup/
set directory=~/.vim/undo/

" Use better tabs
set tabstop=2
set shiftwidth=2

" Keep cursor centered
set so=11

" Easier file management
set autowrite
noremap <F2> :bprevious<CR>
noremap <F3> :bnext<CR>
noremap <F4> :buffers<CR>

" Autosaving 
let g:auto_save = 1  " enable AutoSave on Vim startup
let g:auto_save_in_insert_mode = 1  " do not save while in insert mode

" Enable paste toggle
set pastetoggle=<F5>

" Navigate with <++>
inoremap ½ <Esc>/<++><Enter>"_c4l
vnoremap ½ <Esc>/<++><Enter>"_c4l
map ½ <Esc>/<++><Enter>"_c4l

" Insert <++> using ;½
inoremap ;½ <Esc>i<++>

" Enable wrapping
set wrap

" Traverse wrapped lines
map <silent> <Up> gk
imap <silent> <Up> <C-o>gk
map <silent> <Down> gj
imap <silent> <Down> <C-o>gj
map <silent> <home> g<home>
imap <silent> <home> <C-o>g<home>
map <silent> <End> g<End>
imap <silent> <End> <C-o>g<End>

" Auto Textwidth for certain filetypes
autocmd bufreadpre *.jrnl setlocal textwidth=72
autocmd bufreadpre *.md setlocal textwidth=72

" jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Markdown commands
autocmd bufreadpre *.md inoremap ;c ```<Enter><++><Enter>```<Enter><Enter><++><Esc>4<Up>03<Right>i<Right>
autocmd bufreadpre *.md inoremap ;ic `` <++><Esc>5<Left>i
autocmd bufreadpre *.md inoremap ;d **** <++><Esc>6<Left>i
autocmd bufreadpre *.md inoremap ;l <Enter>- 
autocmd bufreadpre *.md inoremap ;h1 # 
autocmd bufreadpre *.md inoremap ;h2 ## 
autocmd bufreadpre *.md inoremap ;h3 ### 
autocmd bufreadpre *.md inoremap ;h4 #### 
autocmd bufreadpre *.md inoremap ;h5 ##### 
autocmd bufreadpre *.md inoremap ;h6 ###### 
autocmd bufreadpre *.md inoremap ;img ![](<++>)<Esc>6<Left>i
autocmd bufreadpre *.md inoremap ;b **** <++><Esc>6<Left>i
autocmd bufreadpre *.md inoremap ;e **<Left>
autocmd bufreadpre *.md inoremap ;quo > 
autocmd bufreadpre *.md inoremap ;a [](<++>)<Esc>6<Left>i

" Experimental stuff:

" let file_name = expand('%:t:r')
"autocmd bufreadpre *.md noremap <F8> :!./build.sh && pkill -HUP mupdf &<CR><CR>
"autocmd bufreadpre *.md noremap <F9> :!mupdf ccnp-ppt.pdf &<CR><CR>
"autocmd bufreadpre *.md noremap <F10> :!./build.sh; mupdf ccnp-ppt.pdf &<CR><CR>

" autocmd bufreadpre *.md inoremap <F8> <ESC>:!./build.sh && pkill -HUP mupdf &<CR><CR>i<Right>
" autocmd bufreadpre *.md inoremap <F9> <ESC>:!mupdf ccnp-ppt.pdf &<CR><CR>i<Right>
" autocmd bufreadpre *.md inoremap <F10> <ESC>:!./build.sh; mupdf ccnp-ppt.pdf &<CR><CR>i<Right>


" ccnp-ppt.md commands
" autocmd bufreadpre ccnp-ppt.md inoremap ;jm <-->
" autocmd bufreadpre ccnp-ppt.md inoremap ;lo <--><Esc>:0<Enter>/## Lookup<Enter><Down>o- 
" autocmd bufreadpre ccnp-ppt.md inoremap ;gb <Esc>/<--><Enter>"_c4l

" Autoconvert md to pdf on write
" autocmd BufWritePost *.md !pandoc -o <afile>.pdf <afile>
